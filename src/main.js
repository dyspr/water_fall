var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var dimension = 17
var initSize = 0.025
var font
var rain = []
var rainFall = setInterval(makeRain, 1)
var foam = []

function preload() {
  font = loadFont('ttf/RobotoMono-Medium.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < rain.length; i++) {
    rain[i].display()
    rain[i].move()

    if (rain[i].popIt()) {
      foam.push(new Foam())
      rain.splice(i, 1)
    }
  }

  for (var i = 0; i < foam.length; i++) {
    foam[i].display()
    foam[i].move()

    if (foam[i].popIt()) {
      foam.splice(i, 1)
    }
  }
}

function makeRain() {
  for (var i = 0; i < 3; i++) {
    rain.push(new Rain())

  }
}

function makeFoam() {
  foam.push(new Foam())
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

class Foam {
  constructor() {
    var rand = Math.floor(Math.random() * dimension)
    this.x = rand
    this.y = 0.9
    this.size = initSize
    this.speed = 0.005 + Math.random() * 0.002
    this.text = Math.floor(Math.random() * 2)
    this.bg = Math.random()
  }

  display() {
    textFont(font)
    textSize(this.size * boardSize)
    textAlign(CENTER, CENTER)
    if (this.bg < 0.2) {
      fill(255)
      noStroke()
      text(this.text, windowWidth * 0.5 + sin(frameCount * Math.random() * 0.01) * boardSize * initSize * 2 - boardSize * initSize + boardSize * initSize * (this.x - Math.floor(dimension * 0.5)), windowHeight * 0.5 + -0.5 * boardSize + boardSize * this.y)
    } else {
      fill(255)
      noStroke()
      rect(windowWidth * 0.5 + sin(frameCount * Math.random() * 0.01) * boardSize * initSize * 2 - boardSize * initSize + boardSize * initSize * (this.x - Math.floor(dimension * 0.5)), windowHeight * 0.5 + -0.5 * boardSize + boardSize * this.y + this.size * boardSize * 0.2, this.size * boardSize, this.size * boardSize * 1.25)
      fill(0)
      noStroke()
      text(this.text, windowWidth * 0.5 + sin(frameCount * Math.random() * 0.01) * boardSize * initSize * 2 - boardSize * initSize + boardSize * initSize * (this.x - Math.floor(dimension * 0.5)), windowHeight * 0.5 + -0.5 * boardSize + boardSize * this.y)
    }
  }

  move() {
    this.y -= this.speed
  }

  popIt() {
    if (this.y < 0.8) {
      return true
    }
  }
}

class Rain {
  constructor() {
    var rand = Math.floor(Math.random() * dimension)
    this.x = rand
    this.y = 0.1
    this.size = initSize
    this.speed = 0.005 + Math.random() * 0.005
    this.text = Math.floor(Math.random() * 2)
    this.bg = Math.random()
  }

  display() {
    textFont(font)
    textSize(this.size * boardSize)
    textAlign(CENTER, CENTER)
    if (this.bg < 0.95) {
      fill(255)
      noStroke()
      text(this.text, windowWidth * 0.5 + boardSize * initSize * (this.x - Math.floor(dimension * 0.5)), windowHeight * 0.5 + -0.5 * boardSize + boardSize * this.y)
    } else {
      fill(255)
      noStroke()
      rect(windowWidth * 0.5 + boardSize * initSize * (this.x - Math.floor(dimension * 0.5)), windowHeight * 0.5 + -0.5 * boardSize + boardSize * this.y + this.size * boardSize * 0.2, this.size * boardSize, this.size * boardSize * 1.25)
      fill(0)
      noStroke()
      text(this.text, windowWidth * 0.5 + boardSize * initSize * (this.x - Math.floor(dimension * 0.5)), windowHeight * 0.5 + -0.5 * boardSize + boardSize * this.y)
    }
  }

  move() {
    this.y += this.speed
  }

  popIt() {
    if (this.y > 0.85) {
      return true
    }
  }
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
